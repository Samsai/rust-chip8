extern crate sdl2;

use sdl2::audio::{AudioCallback, AudioSpecDesired};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Point;

use std::env;
use std::path::PathBuf;
use std::thread;
use std::time::Duration;

mod chip8;
use chip8::cpu::CPU;
use chip8::utils::get_pixel;

fn sdl_keycode_to_chip8(keycode: sdl2::keyboard::Keycode) -> u8 {
    match keycode {
        Keycode::Num1 => 0x1,
        Keycode::Num2 => 0x2,
        Keycode::Num3 => 0x3,
        Keycode::Num4 => 0xc,
        Keycode::Q => 0x4,
        Keycode::W => 0x5,
        Keycode::E => 0x6,
        Keycode::R => 0xd,
        Keycode::A => 0x7,
        Keycode::S => 0x8,
        Keycode::D => 0x9,
        Keycode::F => 0xe,
        Keycode::Z => 0xa,
        Keycode::X => 0x0,
        Keycode::C => 0xb,
        Keycode::V => 0xf,
        _ => 0,
    }
}

struct SquareWave {
    phase_inc: f32,
    phase: f32,
    volume: f32,
}

impl AudioCallback for SquareWave {
    type Channel = f32;

    fn callback(&mut self, out: &mut [f32]) {
        // Generate a square wave
        for x in out.iter_mut() {
            *x = if self.phase <= 0.5 {
                self.volume
            } else {
                -self.volume
            };
            self.phase = (self.phase + self.phase_inc) % 1.0;
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let user_program = &args[1];

    println!("Loading program: {}", user_program);

    let sdl_context = sdl2::init().unwrap();

    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("Rust CHIP-8", 800, 600)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .unwrap();

    canvas
        .set_logical_size(64, 32)
        .expect("Couldn't set logical size");

    let mut events = sdl_context.event_pump().unwrap();

    let audio_subsystem = sdl_context.audio().unwrap();

    let desired_spec = AudioSpecDesired {
        freq: Some(44100),
        channels: Some(1), // mono
        samples: None,     // default sample size
    };

    let audio_device = audio_subsystem
        .open_playback(None, &desired_spec, |spec| {
            // initialize the audio callback
            SquareWave {
                phase_inc: 1000.0 / spec.freq as f32,
                phase: 0.0,
                volume: 0.10,
            }
        })
        .unwrap();

    let mut cpu = CPU::new();

    let bus = cpu.borrow_bus();

    {
        let mut ram_buffer = bus.get_ram_buffer().lock().unwrap();

        ram_buffer.load_rom(&PathBuf::from(user_program));
    }

    thread::spawn(move || loop {
        cpu.run_cycle();
        thread::sleep(Duration::from_micros(1000));
    });

    let dt_bus = bus.clone();
    let st_bus = bus.clone();

    // Delay timer decrementer
    thread::spawn(move || loop {
        if dt_bus.load_dt() > 0 {
            dt_bus.store_dt(dt_bus.load_dt() - 1);
        }
        thread::sleep(Duration::from_millis(1000 / 60))
    });

    // Sound timer decrementer
    thread::spawn(move || loop {
        if st_bus.load_st() > 0 {
            st_bus.store_st(st_bus.load_st() - 1);
        }
        thread::sleep(Duration::from_millis(1000 / 60))
    });

    'mainloop: loop {
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();

        // Event handling
        for event in events.poll_iter() {
            match event {
                Event::Quit { .. } => break 'mainloop,
                Event::KeyDown {
                    keycode: Some(key), ..
                } => {
                    bus.press_key(sdl_keycode_to_chip8(key) as usize);
                }
                Event::KeyUp {
                    keycode: Some(key), ..
                } => {
                    bus.clear_key(sdl_keycode_to_chip8(key) as usize);
                }
                _ => (),
            }
        }

        canvas.set_draw_color(Color::RGB(255, 255, 255));

        for y in 0..32 {
            for x in 0..64 {
                let pixel = get_pixel(&bus, x, y);
                if pixel > 0 {
                    canvas
                        .draw_point(Point::new(x as i32, y as i32))
                        .expect("Draw failed");
                }
            }
        }

        canvas.present();

        if bus.load_st() > 0 {
            audio_device.resume();
        } else {
            audio_device.pause();
        }
    }
}
