use std::sync::Arc;

use super::bus::Bus;
use super::*;

pub fn get_pixel(bus: &Arc<Bus>, x: u8, y: u8) -> u8 {
    let index: usize = DISPLAY_BUFFER + y as usize * 8 + x as usize / 8;

    let byte = bus.read(index);

    let bit_mask = 128 >> (x & 7);

    return byte & bit_mask;
}

pub fn flip_pixel(bus: &Arc<Bus>, x: u8, y: u8) -> u8 {
    let index: usize = DISPLAY_BUFFER + y as usize * 8 + x as usize / 8;

    let byte = bus.read(index);

    let bit_mask = 128 >> (x & 7);

    bus.write(index, byte ^ bit_mask);

    return byte & bit_mask;
}

pub struct RegisterIterator {
    current: Register,
    done: bool,
}

impl RegisterIterator {
    pub fn new(reg: Register) -> RegisterIterator {
        return RegisterIterator {
            current: reg,
            done: false,
        };
    }
}

impl Iterator for RegisterIterator {
    type Item = Register;

    fn next(&mut self) -> Option<Register> {
        let value = if self.done { None } else { Some(self.current) };

        if self.current == Register::VF {
            self.done = true;
        }

        self.current = match self.current {
            Register::V0 => Register::V1,
            Register::V1 => Register::V2,
            Register::V2 => Register::V3,
            Register::V3 => Register::V4,
            Register::V4 => Register::V5,
            Register::V5 => Register::V6,
            Register::V6 => Register::V7,
            Register::V7 => Register::V8,
            Register::V8 => Register::V9,
            Register::V9 => Register::VA,
            Register::VA => Register::VB,
            Register::VB => Register::VC,
            Register::VC => Register::VD,
            Register::VD => Register::VE,
            Register::VE => Register::VF,
            Register::VF => Register::VF,
        };

        return value;
    }
}

pub fn nibble_to_register(nibble: u8) -> Register {
    match nibble {
        0x0 => return Register::V0,
        0x1 => return Register::V1,
        0x2 => return Register::V2,
        0x3 => return Register::V3,
        0x4 => return Register::V4,
        0x5 => return Register::V5,
        0x6 => return Register::V6,
        0x7 => return Register::V7,
        0x8 => return Register::V8,
        0x9 => return Register::V9,
        0xa => return Register::VA,
        0xb => return Register::VB,
        0xc => return Register::VC,
        0xd => return Register::VD,
        0xe => return Register::VE,
        0xf => return Register::VF,
        _ => return Register::VF,
    }
}
