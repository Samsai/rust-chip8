extern crate rand;

pub mod bus;
pub mod cpu;
pub mod utils;

const DISPLAY_BUFFER: usize = 4096 - 256;
const DISPLAY_WIDTH: u8 = 64;
const DISPLAY_HEIGHT: u8 = 32;

#[derive(Debug, PartialEq)]
pub enum Instruction {
    NOP,
    // BRANCHING
    RET, // Return from subroutine
    JP(u16),
    JPR(u16), // Jump relative to V0
    CALL(u16),
    SEI(Register, u8),       // Skip next if equal
    SNEI(Register, u8),      // Skip next if not equal
    SE(Register, Register),  // Skip next if Registers equal
    SNE(Register, Register), // Skip next if Registers not equal

    // LOAD/STORE
    LDI(Register, u8),      // Load Immediate
    LD(Register, Register), // Load from register
    LDIN(u16),              // Load index
    LDS(Register),          // Load sprite address
    LDB(Register),          // Load BCD
    SCTX(Register),         // Store V0-Vx starting at I
    LCTX(Register),         // Load V0-Vx starting at I

    // Math
    ADI(Register, u8),        // Add immediate
    ADD(Register, Register),  // Add register
    ADDI(Register),           // Add register to index
    SUB(Register, Register),  // Subtract register
    SUBN(Register, Register), // Subtract register but funky

    // Binary logic
    OR(Register, Register),
    AND(Register, Register),
    XOR(Register, Register),
    SHR(Register),
    SHL(Register),

    // Misc
    CLS,                         // Clear screen
    RND(Register, u8),           // Generate random number
    DRW(Register, Register, u8), // Draw sprite
    SKP(Register),               // Input
    SKNP(Register),              // Input
    LDT(Register),               // Load Delay Timer
    LDK(Register),               // Load key
    SDT(Register),               // Store Delay Timer
    SST(Register),               // Store Sound Timer
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Register {
    V0,
    V1,
    V2,
    V3,
    V4,
    V5,
    V6,
    V7,
    V8,
    V9,
    VA,
    VB,
    VC,
    VD,
    VE,
    VF,
}
