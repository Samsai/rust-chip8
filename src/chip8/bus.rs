use std::sync::atomic::AtomicU8;
use std::sync::atomic::Ordering;
use std::sync::Mutex;

use std::path::Path;

type Keyboard = Mutex<[bool; 16]>;

pub struct RAM {
    memory: Vec<u8>,
}

impl RAM {
    pub fn new() -> RAM {
        let mut ram = RAM {
            memory: Vec::with_capacity(4096),
        };

        for _i in 0..4096 {
            ram.memory.push(0);
        }

        let font_data: Vec<u8> = vec![
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80, // F
        ];

        let mut index = 0;

        for byte in font_data {
            ram.memory[index] = byte;
            index += 1;
        }

        return ram;
    }

    pub fn load_rom(&mut self, path: &Path) {
        use std::fs::File;
        use std::io::Read;

        let mut file = File::open(path).expect("Couldn't open file");

        let program_memory = &mut self.memory[0x200..0xfff];

        file.read(program_memory).expect("Failed to read ROM");
    }
}

pub struct Bus {
    ram: Mutex<RAM>,
    keyboard: Keyboard,
    delay_timer: AtomicU8,
    sound_timer: AtomicU8,
}

impl Bus {
    pub fn new() -> Bus {
        return Bus {
            ram: Mutex::new(RAM::new()),
            keyboard: Mutex::new([false; 16]),
            delay_timer: AtomicU8::new(0),
            sound_timer: AtomicU8::new(0),
        };
    }
    pub fn read(&self, index: usize) -> u8 {
        let mem = self.ram.lock().unwrap();

        return mem.memory[index % 4096];
    }

    pub fn write(&self, index: usize, data: u8) {
        let mut mem = self.ram.lock().unwrap();

        mem.memory[index % 4096] = data;
    }

    pub fn get_ram_buffer(&self) -> &Mutex<RAM> {
        return &self.ram;
    }

    pub fn is_key_pressed(&self, index: usize) -> bool {
        let kb = self.keyboard.lock().unwrap();

        return kb[index];
    }

    pub fn press_key(&self, index: usize) {
        let mut kb = self.keyboard.lock().unwrap();

        kb[index] = true;
    }

    pub fn clear_key(&self, index: usize) {
        let mut kb = self.keyboard.lock().unwrap();

        kb[index] = false;
    }

    pub fn load_dt(&self) -> u8 {
        self.delay_timer.load(Ordering::Relaxed)
    }

    pub fn store_dt(&self, value: u8) {
        self.delay_timer.store(value, Ordering::Relaxed)
    }

    pub fn load_st(&self) -> u8 {
        self.sound_timer.load(Ordering::Relaxed)
    }

    pub fn store_st(&self, value: u8) {
        self.sound_timer.store(value, Ordering::Relaxed)
    }
}
