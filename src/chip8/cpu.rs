use super::bus::Bus;
use super::utils::*;
use super::*;

use rand::thread_rng;
use rand::Rng;

use std::sync::Arc;

pub struct CPU {
    bus: Arc<Bus>,
    sp: usize,
    index_reg: usize,
    pc: usize,
}

impl CPU {
    pub fn new() -> CPU {
        return CPU {
            bus: Arc::new(Bus::new()),
            sp: 0xf00 - 17,
            pc: 0x200,
            index_reg: 0,
        };
    }

    pub fn borrow_bus(&self) -> Arc<Bus> {
        return self.bus.clone();
    }

    pub fn read_register(&self, reg: Register) -> u8 {
        match reg {
            Register::V0 => self.bus.read(0xf00 - 17 + 1),
            Register::V1 => self.bus.read(0xf00 - 17 + 2),
            Register::V2 => self.bus.read(0xf00 - 17 + 3),
            Register::V3 => self.bus.read(0xf00 - 17 + 4),
            Register::V4 => self.bus.read(0xf00 - 17 + 5),
            Register::V5 => self.bus.read(0xf00 - 17 + 6),
            Register::V6 => self.bus.read(0xf00 - 17 + 7),
            Register::V7 => self.bus.read(0xf00 - 17 + 8),
            Register::V8 => self.bus.read(0xf00 - 17 + 9),
            Register::V9 => self.bus.read(0xf00 - 17 + 10),
            Register::VA => self.bus.read(0xf00 - 17 + 11),
            Register::VB => self.bus.read(0xf00 - 17 + 12),
            Register::VC => self.bus.read(0xf00 - 17 + 13),
            Register::VD => self.bus.read(0xf00 - 17 + 14),
            Register::VE => self.bus.read(0xf00 - 17 + 15),
            Register::VF => self.bus.read(0xf00 - 17 + 16),
        }
    }

    pub fn write_register(&self, reg: Register, byte: u8) {
        match reg {
            Register::V0 => self.bus.write(0xf00 - 17 + 1, byte),
            Register::V1 => self.bus.write(0xf00 - 17 + 2, byte),
            Register::V2 => self.bus.write(0xf00 - 17 + 3, byte),
            Register::V3 => self.bus.write(0xf00 - 17 + 4, byte),
            Register::V4 => self.bus.write(0xf00 - 17 + 5, byte),
            Register::V5 => self.bus.write(0xf00 - 17 + 6, byte),
            Register::V6 => self.bus.write(0xf00 - 17 + 7, byte),
            Register::V7 => self.bus.write(0xf00 - 17 + 8, byte),
            Register::V8 => self.bus.write(0xf00 - 17 + 9, byte),
            Register::V9 => self.bus.write(0xf00 - 17 + 10, byte),
            Register::VA => self.bus.write(0xf00 - 17 + 11, byte),
            Register::VB => self.bus.write(0xf00 - 17 + 12, byte),
            Register::VC => self.bus.write(0xf00 - 17 + 13, byte),
            Register::VD => self.bus.write(0xf00 - 17 + 14, byte),
            Register::VE => self.bus.write(0xf00 - 17 + 15, byte),
            Register::VF => self.bus.write(0xf00 - 17 + 16, byte),
        }
    }

    #[allow(dead_code)]
    pub fn dump_registers(&self) {
        let register_iter = RegisterIterator::new(Register::V0);

        for reg in register_iter {
            println!("{:?}: {}", reg, self.read_register(reg));
        }
    }

    pub fn run_cycle(&mut self) {
        // println!("PC: {}", self.pc);

        let raw_instruction =
            u16::from_be_bytes([self.bus.read(self.pc), self.bus.read(self.pc + 1)]);

        // println!("RAW: {:#01x}", raw_instruction);

        let instruction = Self::decode_instruction(raw_instruction);

        self.run_instruction(instruction);
    }

    fn decode_instruction(instruction: u16) -> Instruction {
        let nibble1 = ((instruction & 0xF000) >> 12) as u8;
        let nibble2 = ((instruction & 0xF00) >> 8) as u8;
        let nibble3 = ((instruction & 0xF0) >> 4) as u8;
        let nibble4 = (instruction & 0xF) as u8;

        match nibble1 {
            0x0 => {
                if nibble3 == 0xe && nibble4 == 0x0 {
                    return Instruction::CLS;
                } else {
                    return Instruction::RET;
                }
            }
            0x1 => return Instruction::JP(instruction << 4 >> 4),
            0x2 => return Instruction::CALL(instruction << 4 >> 4),
            0x3 => return Instruction::SEI(nibble_to_register(nibble2), (nibble3 << 4) + nibble4),
            0x4 => return Instruction::SNEI(nibble_to_register(nibble2), (nibble3 << 4) + nibble4),
            0x5 => {
                return Instruction::SE(nibble_to_register(nibble2), nibble_to_register(nibble3))
            }
            0x6 => return Instruction::LDI(nibble_to_register(nibble2), (nibble3 << 4) + nibble4),
            0x7 => return Instruction::ADI(nibble_to_register(nibble2), (nibble3 << 4) + nibble4),
            0x8 => match nibble4 {
                0x0 => {
                    return Instruction::LD(
                        nibble_to_register(nibble2),
                        nibble_to_register(nibble3),
                    )
                }
                0x1 => {
                    return Instruction::OR(
                        nibble_to_register(nibble2),
                        nibble_to_register(nibble3),
                    )
                }
                0x2 => {
                    return Instruction::AND(
                        nibble_to_register(nibble2),
                        nibble_to_register(nibble3),
                    )
                }
                0x3 => {
                    return Instruction::XOR(
                        nibble_to_register(nibble2),
                        nibble_to_register(nibble3),
                    )
                }
                0x4 => {
                    return Instruction::ADD(
                        nibble_to_register(nibble2),
                        nibble_to_register(nibble3),
                    )
                }
                0x5 => {
                    return Instruction::SUB(
                        nibble_to_register(nibble2),
                        nibble_to_register(nibble3),
                    )
                }
                0x6 => return Instruction::SHR(nibble_to_register(nibble2)),
                0x7 => {
                    return Instruction::SUBN(
                        nibble_to_register(nibble2),
                        nibble_to_register(nibble3),
                    )
                }
                _ => return Instruction::SHL(nibble_to_register(nibble2)),
            },
            0x9 => {
                return Instruction::SNE(nibble_to_register(nibble2), nibble_to_register(nibble3))
            }
            0xa => return Instruction::LDIN(instruction << 4 >> 4),
            0xb => return Instruction::JPR(instruction << 4 >> 4),
            0xc => return Instruction::RND(nibble_to_register(nibble2), (nibble3 << 4) + nibble4),
            0xd => {
                return Instruction::DRW(
                    nibble_to_register(nibble2),
                    nibble_to_register(nibble3),
                    nibble4,
                )
            }
            0xe => {
                if nibble3 == 0x9 && nibble4 == 0xe {
                    return Instruction::SKP(nibble_to_register(nibble2));
                } else {
                    return Instruction::SKNP(nibble_to_register(nibble2));
                }
            }
            0xf => {
                if nibble3 == 0x0 && nibble4 == 0x7 {
                    return Instruction::LDT(nibble_to_register(nibble2));
                } else if nibble3 == 0x0 && nibble4 == 0xa {
                    return Instruction::LDK(nibble_to_register(nibble2));
                } else if nibble3 == 0x1 && nibble4 == 0x5 {
                    return Instruction::SDT(nibble_to_register(nibble2));
                } else if nibble3 == 0x1 && nibble4 == 0x8 {
                    return Instruction::SST(nibble_to_register(nibble2));
                } else if nibble3 == 0x1 && nibble4 == 0xe {
                    return Instruction::ADDI(nibble_to_register(nibble2));
                } else if nibble3 == 0x2 {
                    return Instruction::LDS(nibble_to_register(nibble2));
                } else if nibble3 == 0x3 && nibble4 == 0x3 {
                    return Instruction::LDB(nibble_to_register(nibble2));
                } else if nibble3 == 0x5 {
                    return Instruction::SCTX(nibble_to_register(nibble2));
                } else {
                    return Instruction::LCTX(nibble_to_register(nibble2));
                }
            }
            _ => return Instruction::NOP,
        }
    }

    pub fn run_instruction(&mut self, instruction: Instruction) {
        // self.dump_registers();
        self.pc += 2;
        // println!("{:?}", instruction);
        match instruction {
            Instruction::NOP => (),

            Instruction::RET => {
                self.pc = self.bus.read(self.sp) as usize;
                self.sp += 1;
                self.pc <<= 8;
                self.pc += self.bus.read(self.sp) as usize;
                self.sp += 1;
            }
            Instruction::JP(addr) => self.pc = addr as usize,
            Instruction::JPR(addr) => {
                self.pc = addr as usize + self.read_register(Register::V0) as usize
            }
            Instruction::CALL(addr) => {
                let bytes = self.pc.to_be_bytes();

                self.sp -= 1;
                self.bus.write(self.sp, bytes[7]);
                self.sp -= 1;
                self.bus.write(self.sp, bytes[6]);

                self.pc = addr as usize;
            }
            Instruction::SEI(reg, value) => {
                if self.read_register(reg) == value {
                    self.pc += 2;
                }
            }
            Instruction::SNEI(reg, value) => {
                if self.read_register(reg) != value {
                    self.pc += 2;
                }
            }
            Instruction::SE(reg1, reg2) => {
                if self.read_register(reg1) == self.read_register(reg2) {
                    self.pc += 2;
                }
            }
            Instruction::SNE(reg1, reg2) => {
                if self.read_register(reg1) != self.read_register(reg2) {
                    self.pc += 2;
                }
            }

            // LOAD/STORE
            Instruction::LDI(reg, value) => self.write_register(reg, value),
            Instruction::LD(reg1, reg2) => self.write_register(reg1, self.read_register(reg2)),
            Instruction::LDIN(value) => self.index_reg = value as usize,
            Instruction::LDS(reg) => {
                // Each font has length of 5 bytes, load the nth starting from addr 0
                self.index_reg = (self.read_register(reg) * 5) as usize;
            }
            Instruction::LDB(reg) => {
                let value = self.read_register(reg);
                let hundreds = value / 100;
                let tens = (value - hundreds * 100) / 10;
                let ones = (value - hundreds * 100) - tens * 10;

                self.bus.write(self.index_reg, hundreds);
                self.bus.write(self.index_reg + 1, tens);
                self.bus.write(self.index_reg + 2, ones);
            }
            Instruction::SCTX(reg) => {
                let mut registers = RegisterIterator::new(Register::V0);
                let mut current_register;
                let mut offset = 0;

                loop {
                    current_register = registers.next().unwrap();
                    self.bus.write(
                        self.index_reg + offset,
                        self.read_register(current_register),
                    );
                    offset += 1;

                    if current_register == reg {
                        break;
                    }
                }
            }
            Instruction::LCTX(reg) => {
                let mut registers = RegisterIterator::new(Register::V0);
                let mut current_register;
                let mut offset = 0;

                loop {
                    current_register = registers.next().unwrap();
                    self.write_register(current_register, self.bus.read(self.index_reg + offset));
                    offset += 1;

                    if current_register == reg {
                        break;
                    }
                }
            }

            // Math
            Instruction::ADI(reg, value) => {
                let (new_value, overflow) = self.read_register(reg).overflowing_add(value);
                self.write_register(reg, new_value);

                if overflow {
                    self.write_register(Register::VF, 1);
                } else {
                    self.write_register(Register::VF, 0);
                }
            }
            Instruction::ADD(reg1, reg2) => {
                let (new_value, overflow) = self
                    .read_register(reg1)
                    .overflowing_add(self.read_register(reg2));

                self.write_register(reg1, new_value);

                if overflow {
                    self.write_register(Register::VF, 1);
                } else {
                    self.write_register(Register::VF, 0);
                }
            }
            Instruction::ADDI(reg) => self.index_reg += self.read_register(reg) as usize,
            Instruction::SUB(reg1, reg2) => {
                let (new_value, overflow) = self
                    .read_register(reg1)
                    .overflowing_sub(self.read_register(reg2));

                self.write_register(reg1, new_value);

                if overflow {
                    self.write_register(Register::VF, 1);
                } else {
                    self.write_register(Register::VF, 0);
                }
            }
            Instruction::SUBN(reg1, reg2) => {
                let (new_value, overflow) = self
                    .read_register(reg1)
                    .overflowing_sub(self.read_register(reg2));

                self.write_register(reg2, new_value);

                if overflow {
                    self.write_register(Register::VF, 1);
                } else {
                    self.write_register(Register::VF, 0);
                }
            }

            // // Binary logic
            Instruction::OR(reg1, reg2) => {
                self.write_register(reg1, self.read_register(reg1) | self.read_register(reg2));
            }
            Instruction::AND(reg1, reg2) => {
                self.write_register(reg1, self.read_register(reg1) & self.read_register(reg2));
            }
            Instruction::XOR(reg1, reg2) => {
                self.write_register(reg1, self.read_register(reg1) ^ self.read_register(reg2));
            }
            Instruction::SHR(reg) => {
                // Store least significant bit
                self.write_register(Register::VF, self.read_register(reg) & 0x01);
                self.write_register(reg, self.read_register(reg) >> 1);
            }
            Instruction::SHL(reg) => {
                // Store least significant bit
                self.write_register(Register::VF, self.read_register(reg) & 0x80);
                self.write_register(reg, self.read_register(reg) << 1);
            }

            // Misc
            Instruction::CLS => {
                for i in 0..256 {
                    self.bus.write(DISPLAY_BUFFER + i, 0);
                }
            }
            Instruction::RND(reg, byte) => {
                let random_num = thread_rng().gen_range(0..256) as u8 & byte;
                self.write_register(reg, random_num);
            }
            Instruction::DRW(reg1, reg2, byte) => {
                let xcoord = self.read_register(reg1) % DISPLAY_WIDTH;
                let ycoord = self.read_register(reg2) % DISPLAY_HEIGHT;

                self.write_register(Register::VF, 0);

                for row in 0..byte {
                    let sprite_bits = self.bus.read(self.index_reg + row as usize);

                    let cy = (ycoord + row) % DISPLAY_HEIGHT;

                    for col in 0..8 {
                        let cx = xcoord + col % DISPLAY_WIDTH;

                        let pixel = sprite_bits & (0x01 << 7 - col);

                        if pixel > 0 {
                            let flip = flip_pixel(&self.bus, cx, cy);

                            if flip > 0 {
                                self.write_register(Register::VF, 1);
                            }
                        }
                    }
                }
            }
            Instruction::SKP(reg) => {
                if self.bus.is_key_pressed(self.read_register(reg) as usize) {
                    self.pc += 2;
                }
            }
            Instruction::SKNP(reg) => {
                if !self.bus.is_key_pressed(self.read_register(reg) as usize) {
                    self.pc += 2;
                }
            }
            Instruction::LDK(reg) => {
                'wait: loop {
                    for i in 0..16 {
                        if self.bus.is_key_pressed(i) {
                            self.write_register(reg, i as u8);
                            break 'wait;
                        }
                        std::thread::sleep(std::time::Duration::from_millis(1));
                    }
                }
            }
            Instruction::LDT(reg) => {
                self.write_register(reg, self.bus.load_dt());
            }
            Instruction::SDT(reg) => {
                self.bus.store_dt(self.read_register(reg));
            }
            Instruction::SST(reg) => {
                self.bus.store_st(self.read_register(reg));
            }
        }
    }
}

#[cfg(test)]
mod cpu_tests {
    use super::*;

    #[test]
    fn immediate_loads_decode_correctly() {
        let mut cpu = CPU::new();

        for i in 0..0xf {
            let register_nibble: u16 = i << (4 * 2);
            let raw_instruction = (6 << (4 * 3)) + register_nibble + 69;

            let instruction = CPU::decode_instruction(raw_instruction);

            assert_eq!(
                Instruction::LDI(nibble_to_register(i as u8), 69),
                instruction
            );

            cpu.run_instruction(instruction);

            assert_eq!(cpu.read_register(nibble_to_register(i as u8)), 69);
        }
    }

    #[test]
    fn sne_correctly_decoded() {
        let instruction = 0x9560;

        assert_eq!(
            Instruction::SNE(Register::V5, Register::V6),
            CPU::decode_instruction(instruction)
        );
    }

    #[test]
    fn executing_instruction_increments_pc() {
        let mut cpu = CPU::new();

        assert_eq!(cpu.pc, 0x200);

        cpu.run_instruction(Instruction::LDI(Register::V0, 69));

        assert_eq!(cpu.pc, 0x202);
    }

    #[test]
    fn jump_sets_pc() {
        let mut cpu = CPU::new();

        assert_eq!(cpu.pc, 0x200);

        cpu.run_instruction(Instruction::JP(0x500));

        assert_eq!(cpu.pc, 0x500);
    }

    #[test]
    fn storing_and_loading_context_works() {
        let mut cpu = CPU::new();

        cpu.run_instruction(Instruction::LDI(Register::V0, 0));
        cpu.run_instruction(Instruction::LDI(Register::V1, 1));
        cpu.run_instruction(Instruction::LDI(Register::V2, 2));
        cpu.run_instruction(Instruction::LDI(Register::V3, 3));
        cpu.run_instruction(Instruction::LDI(Register::V4, 4));
        cpu.run_instruction(Instruction::LDI(Register::V5, 5));
        cpu.run_instruction(Instruction::LDI(Register::V6, 6));

        cpu.run_instruction(Instruction::SCTX(Register::V6));

        cpu.run_instruction(Instruction::LDI(Register::V0, 9));
        cpu.run_instruction(Instruction::LDI(Register::V1, 9));
        cpu.run_instruction(Instruction::LDI(Register::V2, 9));
        cpu.run_instruction(Instruction::LDI(Register::V3, 9));
        cpu.run_instruction(Instruction::LDI(Register::V4, 9));
        cpu.run_instruction(Instruction::LDI(Register::V5, 9));
        cpu.run_instruction(Instruction::LDI(Register::V6, 9));

        cpu.run_instruction(Instruction::LCTX(Register::V6));

        let mut register_iterator = RegisterIterator::new(Register::V0);

        for i in 0..=6 {
            assert_eq!(cpu.read_register(register_iterator.next().unwrap()), i);
        }
    }

    #[test]
    fn call_and_return_matching_pc() {
        let mut cpu = CPU::new();

        cpu.run_instruction(Instruction::JP(702));

        cpu.run_instruction(Instruction::CALL(578));

        assert_eq!(cpu.pc, 578);

        cpu.run_instruction(Instruction::LDI(Register::V0, 69));

        cpu.run_instruction(Instruction::RET);

        assert_eq!(cpu.pc, 704);
    }

    #[test]
    fn flipping_top_left_pixels_works() {
        let mut cpu = CPU::new();

        cpu.run_instruction(Instruction::CLS);

        flip_pixel(&cpu.bus, 0, 0);

        assert_eq!(cpu.bus.read(DISPLAY_BUFFER), 0b10000000);

        flip_pixel(&cpu.bus, 5, 0);

        assert_eq!(cpu.bus.read(DISPLAY_BUFFER), 0b10000100);
    }

    #[test]
    fn get_pixel_finds_correct_pixel() {
        let mut cpu = CPU::new();

        cpu.run_instruction(Instruction::CLS);

        assert!(get_pixel(&cpu.bus, 0, 0) == 0);

        flip_pixel(&cpu.bus, 0, 0);

        assert!(get_pixel(&cpu.bus, 0, 0) > 0);

        flip_pixel(&cpu.bus, 5, 0);

        assert!(get_pixel(&cpu.bus, 5, 0) > 0);
    }

    #[test]
    fn flipping_same_pixel_raises_flag() {
        let mut cpu = CPU::new();

        cpu.run_instruction(Instruction::CLS);

        flip_pixel(&cpu.bus, 0, 0);
        assert!(flip_pixel(&cpu.bus, 0, 0) > 0);
    }

    #[test]
    fn flipping_different_pixel_no_flag() {
        let mut cpu = CPU::new();

        cpu.run_instruction(Instruction::CLS);

        flip_pixel(&cpu.bus, 0, 0);
        assert!(flip_pixel(&cpu.bus, 0, 1) == 0);
    }

    #[test]
    fn draw_collision_repored() {
        let mut cpu = CPU::new();

        cpu.run_instruction(Instruction::CLS);

        flip_pixel(&cpu.bus, 0, 0);

        assert!(flip_pixel(&cpu.bus, 0, 0) > 0);
    }

    #[test]
    fn cpu_reacts_to_keyboard_presses() {
        let mut cpu = CPU::new();

        let bus = cpu.borrow_bus();

        bus.press_key(0);

        cpu.run_instruction(Instruction::LDI(Register::V0, 0));
        cpu.run_instruction(Instruction::JP(500));
        cpu.run_instruction(Instruction::SKP(Register::V0));

        assert_eq!(504, cpu.pc);
    }

    #[test]
    fn no_false_reaction_to_keyboard_presses() {
        let mut cpu = CPU::new();

        let bus = cpu.borrow_bus();

        bus.press_key(1);

        cpu.run_instruction(Instruction::LDI(Register::V0, 0));
        cpu.run_instruction(Instruction::JP(500));
        cpu.run_instruction(Instruction::SKP(Register::V0));

        assert_eq!(502, cpu.pc);
    }

    #[test]
    fn proper_value_gets_read() {
        let mut cpu = CPU::new();

        let bus = cpu.borrow_bus();

        bus.press_key(0xa);

        cpu.run_instruction(Instruction::LDK(Register::V0));

        assert_eq!(0xa, cpu.read_register(Register::V0));
    }
}
